This project was bootstrapped with [Create React DynamicApp](https://github.com/facebook/create-react-app).

## REACT MOVIE APP

### Instruction

1. Check your Internet connection. 

2. In the project directory, run:

    ```
    npm install
    npm start
    ```

3. Open [http://localhost:3000](http://localhost:3000) to view it in the browser.
You will see ***static*** content with local data.

4. To fetch the data of 100 movies, enter your movie api key and click **'Download data...'** button. You will see ***dynamic*** content with fetched data. 


