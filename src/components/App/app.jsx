import React, {Component} from 'react';
import './app.css';
import DynamicMovieTable from "../DynamicMovieTable/dynamicMovieTable"
import StaticMovieTable from "../StaticMovieTable/staticMovieTable"

class App extends Component {
    state = {
        isStatic: true
    };
    changeStatic = () => {
        this.setState({isStatic: !this.state.isStatic});
    };

    render() {
        return (
            <div className="App">
                <h1 style={{textAlign: 'center'}}>{this.state.isStatic ? 'Static ' : 'Dynamic '} React App</h1>
                <div style={{marginBottom: '15px'}}>
                    <div>
                        <div style={{marginBottom: '15px'}}>
                            <label>Enter yout Api key: </label>
                            <input id="inputApiKey"/>
                        </div>
                        <button onClick={this.changeStatic}>Download data...</button>
                    </div>
                </div>
                {this.state.isStatic ? <StaticMovieTable/> : <DynamicMovieTable/>}
            </div>
        );
    }
}

export default App;