import React, {Component} from 'react';
import './appError.css';
import ReactDOM from "react-dom";
import App from "../App/app";

class AppError extends Component {
    render() {
        let error = this.props.error;
        return (
            <main>
                <div>
                    <h1>{error.toString()}</h1>
                    <button onClick={makeStartPage}>Go Back</button>
                </div>
            </main>
        );
    }
}

function makeStartPage() {
    ReactDOM.render(<App/>, document.getElementById('root'));
}

export function makeErrorPage(e) {
    ReactDOM.render(<AppError error={e}/>, document.getElementById('root'));
}