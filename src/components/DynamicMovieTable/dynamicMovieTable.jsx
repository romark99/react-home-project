import React, {Component} from 'react';
import './dynamicMovieTable.css';
import Movie from "../Movie/movie"
import fetchMovies from "../../requests/getMovies";

class DynamicMovieTable extends Component {
    state = {
        movies: []
    };

    render() {
        return (
            <div className="DynamicMovieTable">
                <table>
                    <tbody>
                    <tr>
                        <th>#</th>
                        <th>Id</th>
                        <th>Title</th>
                        <th>Poster</th>
                        <th>Release date</th>
                        <th>Adult</th>
                        <th>Overview</th>
                    </tr>
                    {this.state.movies.map(movie => <Movie key={movie.id} value={movie}/>)}
                    </tbody>
                </table>
            </div>
        );
    }

    componentDidMount() {
        fetchMovies(this, 1);
        fetchMovies(this, 2);
        fetchMovies(this, 3);
        fetchMovies(this, 4);
        fetchMovies(this, 5);
    }
}

export default DynamicMovieTable;
