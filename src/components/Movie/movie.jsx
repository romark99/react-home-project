import React, {Component} from 'react';
import './movie.css';

const movieUrl = 'http://image.tmdb.org/t/p/w185/';

class Movie extends Component {
    state = this.props.value;
    render() {
        return (
            <tr>
                <td>{this.state.number}</td>
                <td>{this.state.id}</td>
                <td>{this.state.title}</td>
                <td><img alt='poster' src={movieUrl + this.state.poster}/></td>
                <td>{this.state.releaseDate}</td>
                <td>{this.state.adult ? 'Yes' : 'No'}</td>
                <td>{this.state.overview}</td>
            </tr>
        );
    }
}

export default Movie;
