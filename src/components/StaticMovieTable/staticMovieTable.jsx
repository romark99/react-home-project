import React, {Component} from 'react';
import './staticMovieTable.css';

class StaticMovieTable extends Component {
    render() {
        return (
            <div className="StaticMovieTable">
                <div style={{overflowX: 'auto'}}>
                    <table>
                        <tbody>
                        <tr>
                            <th>#</th>
                            <th>Id</th>
                            <th>Title</th>
                            <th>Poster</th>
                            <th>Release date</th>
                            <th>Adult</th>
                            <th>Overview</th>
                        </tr>
                        <tr>
                            <td>1</td>
                            <td>19404</td>
                            <td>Dilwale Dulhania Le Jayenge</td>
                            <td><img alt='poster' src="logo192.png"/></td>
                            <td>1995-10-20</td>
                            <td>No</td>
                            <td>Raj is a rich, carefree, happy-go-lucky second generation NRI. Simran is the daughter of
                                Chaudhary Baldev Singh, who in spite of being an NRI is very strict about adherence to
                                Indian values. Simran has left for India to be married to her childhood fiancé. Raj
                                leaves
                                for India with a mission at his hands, to claim his lady love under the noses of her
                                whole
                                family. Thus begins a saga.
                            </td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td>278</td>
                            <td>The Shawshank Redemption</td>
                            <td><img alt='poster' src="logo192.png"/></td>
                            <td>1994-09-23</td>
                            <td>No</td>
                            <td>Framed in the 1940s for the double murder of his wife and her lover, upstanding banker
                                Andy
                                Dufresne begins a new life at the Shawshank prison, where he puts his accounting skills
                                to
                                work for an amoral warden. During his long stretch in prison, Dufresne comes to be
                                admired
                                by the other inmates -- including an older prisoner named Red -- for his integrity and
                                unquenchable sense of hope.
                            </td>
                        </tr>
                        <tr>
                            <td>3</td>
                            <td>238</td>
                            <td>The Godfather</td>
                            <td><img alt='poster' src="logo192.png"/></td>
                            <td>1972-03-15</td>
                            <td>Yes</td>
                            <td>Spanning the years 1945 to 1955, a chronicle of the fictional Italian-American Corleone
                                crime family. When organized crime family patriarch, Vito Corleone barely survives an
                                attempt on his life, his youngest son, Michael steps in to take care of the would-be
                                killers, launching a campaign of bloody revenge.
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        );
    }
}

export default StaticMovieTable;
