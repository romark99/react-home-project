import {makeErrorPage} from "../components/AppError/appError";

const getMoviesUrlByKeyAndPage = (apiKey, page) => {
    return 'https://api.themoviedb.org/3/movie/top_rated?api_key=' + apiKey + '&language=en-US&page=' + page;
};

function* generateNumber() {
    let n = 1;
    while (true) {
        yield n++;
        if (n > 100) n = 1;
    }
}

const generator = generateNumber();

function fetchMovies(context, page) {
    const apiKey = document.getElementById("inputApiKey").value;
    fetch(getMoviesUrlByKeyAndPage(apiKey, page))
        .then(function (response) {
            if (response.status > 100 && response.status <= 400)
                return response.json();
            else if (response.status === 404) {
                throw new Error("Error 404: NOT FOUND");
            } else if (response.status > 400 && response.status < 500) {
                throw new Error("Error " + response.status + ": SOME CLIENT ERROR");
            } else if (response.status > 500 && response.status < 600) {
                throw new Error("Error " + response.status + ": SOME SERVER ERROR");
            } else {
                throw new Error("Error " + response.status + ": UNEXPECTED STATE");
            }
        })
        .then(responseJSON => responseJSON.results)
        .then(results => results.map(result => {
            return {
                'number': generator.next().value,
                'id': result.id,
                'adult': result.adult,
                'title': result.title,
                'poster': result.poster_path,
                'overview': result.overview,
                'releaseDate': result.release_date
            }
        }))
        .then(results => context.setState({movies: context.state.movies.concat(results)}))
        .catch(e => makeErrorPage(e));
}


export default fetchMovies;
